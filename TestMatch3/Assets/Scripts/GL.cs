﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class c_Save {//класс для сохранения данных
    public int[,] aField;
    public bool[,] aMoved;
    public int Score;
}

public static class GL {//ГЛобальный статический класс для быстрых ссылок
    public static c_Save pSave = new c_Save();
    public static eGameState GameState = eGameState.None;
    public static int MaxX = 6;
    public static int MaxY = 6;

    //ссылки на основные объекты
    public static cGameCtl pGameCtl; //управление игрой
    public static cPrefabCtl pPrefabCtl; //управление префабами
    public static cFieldCtl pFieldCtl; //управление полем с фишками
    
}
