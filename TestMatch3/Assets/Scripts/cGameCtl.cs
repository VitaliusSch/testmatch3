﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class cGameCtl : MonoBehaviour {//класс для управления игрой

    public Text teScore;

    public GameObject goAlert;


    void OnEnable() {
        if (GL.pGameCtl == null) {
            GL.pGameCtl = this;
            StartCoroutine(ieLoadMainScene());
        }
    }

    IEnumerator ieLoadMainScene() {
        while (!SceneManager.GetActiveScene().isLoaded) {//ждём загрузки
            yield return new WaitForSeconds(.1f);
        }

        yield return new WaitForEndOfFrame(); 
        yield return new WaitForEndOfFrame();//пропускаем фрэйм, чтобы подцепились ссылки в GL
        if (GL.GameState == eGameState.None) {
            Init();
        }

    }

    void Init() {//инициализация игры
        GL.GameState = eGameState.Init;

        goAlert.SetActive(false);
        teScore.text = "0";
        GL.pSave.Score = 0;

        GL.pPrefabCtl.Init();
        GL.pFieldCtl.Init();

        GL.GameState = eGameState.Fill;
        GL.pFieldCtl.RefreshField();
        GL.GameState = eGameState.Play;

    }

    public void ScoreAdd(int AddCnt) {
        int Key = 23 * 5 + 78;
        GL.pSave.Score += AddCnt * Key; //защищаем память от взлома
        teScore.text = (GL.pSave.Score / Key).ToString();
    }

    public void DoExit() {
        Application.Quit();
    }

    public void DoAlert() {
        Application.Quit();
    }

    IEnumerator ieAlert() {
        goAlert.SetActive(true);
        yield return new WaitForSeconds(2f);
        goAlert.SetActive(false);
    }
}
