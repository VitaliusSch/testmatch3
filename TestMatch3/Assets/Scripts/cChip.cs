﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cChip : MonoBehaviour {
    bool bSelected;
    public GameObject SelectImg;

    public Vector2 vFieldPosition;
    public Vector3 vTarget;

    float MoveSpd = 600;

    Transform tr;
    public int ChipId;

    public void Init(Vector2 FieldPosition, int pChipId) {
        SelectImg.SetActive(false);
        vFieldPosition = FieldPosition;
        tr = transform;
        ChipId = pChipId;
    }

    void FixedUpdate() {
        if (GL.GameState != eGameState.Swap) return;
        if (GL.pSave.aMoved[(int)vFieldPosition.x, (int)vFieldPosition.y]) return;
        //перемещаем фишку до цели
        float step = MoveSpd * Time.fixedDeltaTime;
        tr.localPosition = Vector3.MoveTowards(tr.localPosition, vTarget, step);
        //если фишка на нужной позиции, то помечаем окончание передвижения
        if (tr.localPosition == vTarget) {
            GL.pSave.aMoved[(int)vFieldPosition.x, (int)vFieldPosition.y] = true;
            GL.pSave.aField[(int)vFieldPosition.x, (int)vFieldPosition.y] = ChipId;
            GL.pFieldCtl.aChips[(int)vFieldPosition.x, (int)vFieldPosition.y] = this;
        }
    }

     public void Select() {//выделяем фишку
        if (GL.GameState != eGameState.Play) return;
        bSelected = true;
        SelectImg.SetActive(true);
        GL.pFieldCtl.CheckSelect(this);
    }

    public void UnSelect() {//снимаем выделение
        bSelected = true;
        SelectImg.SetActive(false);
    }

    public void SetTarget(Vector2 Target, Vector2 FieldPosition) {//устанавливаем место назначения
        vTarget = Target;
        vFieldPosition = FieldPosition;
        GL.pSave.aMoved[(int)vFieldPosition.x, (int)vFieldPosition.y] = false;
    }

}
