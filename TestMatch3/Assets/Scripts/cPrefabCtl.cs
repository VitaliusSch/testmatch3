﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cPrefabCtl : MonoBehaviour {//класс для управления префабами

    public GameObject goForHide;
    public GameObject[] aPrefabChips; //массив префабоф фишек

    void OnEnable() {
        if (GL.pPrefabCtl == null) GL.pPrefabCtl = this;
    }

    public GameObject SpawnObj(GameObject cPrefab, Vector3 cPos, Quaternion cRot, Vector3 clocalScale, Transform cParent) {
        Vector3 clocalScale1 = clocalScale;
        if (clocalScale1 == Vector3.zero) clocalScale1 = cPrefab.transform.localScale;
        Quaternion tmpRot = cRot;
        if (tmpRot == Quaternion.identity) tmpRot = cPrefab.transform.localRotation;
        GameObject currObj;
        cPoolCtl tmpPoolCtl = cPrefab.GetComponent<cPoolCtl>();
        if (tmpPoolCtl != null)         //создаем объект из префаба, если нет этого объекта в пуле.
            currObj = tmpPoolCtl.GetPooledInstance(null);
        else
            currObj = GameObject.Instantiate(cPrefab) as GameObject;

        currObj.SetActive(true);
        currObj.transform.parent = cParent;
        currObj.transform.localScale = clocalScale1;
        currObj.transform.localPosition = cPos;
        currObj.transform.localRotation = tmpRot;
        return currObj;
    }


    public void Init () {
        goForHide.SetActive(false);
    }

}
