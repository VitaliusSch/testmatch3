﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cFieldCtl : MonoBehaviour {//класс для управления полем с фишками

    public Transform ChipParent; //родитель, в котором будут создаваться и передвигаться фишки
    public Transform ChipHolderParent; //родитель, в котором хранится разметка поля?
    public cChip[,] aChips;

    public cChip SelectedChip;
    public cChip ToSwapChip;


    void OnEnable() {
        if (GL.pFieldCtl == null) GL.pFieldCtl = this;
    }

    public void Init() {// инициализируем поле фишек
        GL.pSave.aField = new int[GL.MaxX, GL.MaxY];
        GL.pSave.aMoved = new bool[GL.MaxX, GL.MaxY];
        aChips = new cChip[GL.MaxX, GL.MaxY];
        for (int x = 0; x < GL.MaxX; x++) {
            for (int y = 0; y < GL.MaxY; y++) {
                GL.pSave.aMoved[x, y] = true;
                GL.pSave.aField[x, y] = GetRandomChip(x, y);

            }
        }

    }

    bool CanUse(int x, int y, int ChipId) {// проверяем совпадение с соседями у заданной фишки
        bool tmpB = true;
        if (x - 1 >= 0) if (GL.pSave.aField[x - 1, y] == ChipId) tmpB = false;
        if (x + 1 < GL.MaxX) if (GL.pSave.aField[x + 1, y] == ChipId) tmpB = false;
        if (y - 1 >= 0) if (GL.pSave.aField[x, y - 1] == ChipId) tmpB = false;
        if (y + 1 < GL.MaxY) if (GL.pSave.aField[x, y + 1] == ChipId) tmpB = false;
        return tmpB;
    }

    int GetRandomChip(int x, int y) {// Получаем случайную фишку 
        int tmpChipId = Random.Range(0, GL.pPrefabCtl.aPrefabChips.Length);
        if (tmpChipId == 0) tmpChipId = 1;
        if (!CanUse(x, y, tmpChipId)) //если есть совпадения с соседями, то рекурсия
            return GetRandomChip(x, y);
        else 
            return tmpChipId;
    }

    public void RefreshField() {// заполняем поле объектами
        for (int x = 0; x < GL.MaxX; x++) {
            for (int y = 0; y < GL.MaxY; y++) {
                if (GL.pSave.aField[x, y] != -1) {
                    if (aChips[x, y] == null) {
                        SpawnChip(x, y);
                    }
                }
            }
        }

    }

    void SpawnChip(int x, int y) {
        GameObject tmpGo = GL.pPrefabCtl.SpawnObj(GL.pPrefabCtl.aPrefabChips[GL.pSave.aField[x, y]], Vector3.zero, Quaternion.identity, Vector3.zero, ChipParent);
        aChips[x, y] = tmpGo.GetComponent<cChip>();
        tmpGo.transform.localPosition = new Vector2(x * 62, y * 62);
        aChips[x, y].Init(new Vector2(x, y), GL.pSave.aField[x, y]);
    }

    bool CanSwap(cChip Chip1, cChip Chip2) {// проверяем являются ли фишки соседями
        bool tmpB = false;
        if (Chip1.vFieldPosition.x == Chip2.vFieldPosition.x && Mathf.Abs(Chip1.vFieldPosition.y - Chip2.vFieldPosition.y) == 1) tmpB = true;
        if (Chip1.vFieldPosition.y == Chip2.vFieldPosition.y && Mathf.Abs(Chip1.vFieldPosition.x - Chip2.vFieldPosition.x) == 1) tmpB = true;
        return tmpB;
    }

    public void CheckSelect(cChip NewChip) {// Проверяем выделенную фишку
        if (SelectedChip == null) {//если текущая не выбрана, то выбираем
            SelectedChip = NewChip;
            return;
        }

        if (SelectedChip != null && CanSwap(SelectedChip, NewChip)) {//если текущая выбрана и нажата соседняя, то свапаем
            ToSwapChip = NewChip;
            SelectedChip.UnSelect();
            ToSwapChip.UnSelect();
            GL.GameState = eGameState.Swap;
            StartCoroutine(DoSwap());
        } else {//иначе выбираем новую
            SelectedChip.UnSelect();
            SelectedChip = NewChip;
        }

    }

    bool AllChipsMoved() {//проверяем все ли фишки закончили движение
        bool tmpB = true;
        for (int x = 0; x < GL.MaxX; x++) {
            for (int y = 0; y < GL.MaxY; y++) {
                if (!GL.pSave.aMoved[x, y]) tmpB = false;
            }
        }
        return tmpB;
    }

    bool AllChipsFilled() {//проверяем все ли фишки заполненны
        bool tmpB = true;
        for (int x = 0; x < GL.MaxX; x++) {
            for (int y = 0; y < GL.MaxY; y++) {
                if (GL.pSave.aField[x, y] == -1) tmpB = false;
            }
        }
        return tmpB;
    }

    bool NoRows() {//проверяем на невозможность образования ряда
        for (int x = 0; x < GL.MaxX; x++) {
            for (int y = 0; y < GL.MaxY; y++) {
                if (CheckPattern(x, y, x - 1, y + 1, x - 1, y - 1)) return true;//проверяем паттерн на одну клетку влево
                if (CheckPattern(x, y, x - 2, y + 1, x - 2, y + 2) || CheckPattern(x, y, x - 2, y - 1, x - 2, y - 2)) return true;//проверяем паттерны на две клетки влево
                if (CheckPattern(x, y, x - 2, y + 2, x - 3, y + 2) || CheckPattern(x, y, x - 2, y - 2, x - 3, y - 2) || CheckPattern(x, y, x - 2, y, x - 3, y)) return true;//проверяем паттерны на три клетки влево
                if (CheckPattern(x, y, x + 1, y + 1, x + 1, y - 1)) return true;//проверяем паттерн на одну клетку вправо
                if (CheckPattern(x, y, x + 2, y + 1, x + 2, y + 2) || CheckPattern(x, y, x + 2, y - 1, x + 2, y - 2)) return true;//проверяем паттерны на две клетки вправо
                if (CheckPattern(x, y, x + 2, y + 2, x + 3, y + 2) || CheckPattern(x, y, x + 2, y - 2, x + 3, y - 2) || CheckPattern(x, y, x + 2, y, x + 3, y)) return true;//проверяем паттерны на три клетки вправо
                if (CheckPattern(x, y, x + 1, y + 1, x - 1, y + 1)) return true;//проверяем паттерн на одну клетку вверх
                if (CheckPattern(x, y, x, y + 2, x, y + 3)) return true;//проверяем паттерн три клетки вверх
                if (CheckPattern(x, y, x + 1, y - 1, x - 1, y - 1)) return true;//проверяем паттерн на одну клетку вниз
                if (CheckPattern(x, y, x, y - 2, x, y - 3)) return true;//проверяем паттерн три клетки вниз
            }
        }
        return false;
    }

    bool CheckPattern(int x, int y, int x1, int y1, int x2, int y2) {
        if (x1 >= GL.MaxX || x2 >= GL.MaxX || x1 < 0 || x2 < 0 || y1 >= GL.MaxY || y2 >= GL.MaxY || y1 < 0 || y2 < 0 ) return false; //если выходим за границы, то нет совпадений

        if (GL.pSave.aField[x, y] == GL.pSave.aField[x1, y1] && GL.pSave.aField[x, y] == GL.pSave.aField[x2, y2]) return true;//сравниваем ИД проверяемых фишек
        else return false;
    }

    void SwapRandom(int x, int y) {
        if (x + 1 >= GL.MaxX || x - 1 < 0 || y + 1 >= GL.MaxY || y - 1 < 0) return; //если выходим за границы, то нет меняем
        int x1 = x;
        int y1 = y;
        if (Random.Range(0, 100) < 50) x1++; else x1--;//свыбираем случайную фишку
        if (Random.Range(0, 100) < 50) y1++; else y1--;
        int TmpChipId = GL.pSave.aField[x, y];
        GL.pSave.aField[x, y] = GL.pSave.aField[x1, y1]; //меняем местами
        GL.pSave.aField[x1, y1] = TmpChipId;
    }

    void DoMix() {//перемешиваем фишки
        GL.pGameCtl.DoAlert();
        for (int x = 0; x < GL.MaxX; x++) {
            for (int y = 0; y < GL.MaxY; y++) {
                if (aChips[x, y] != null) aChips[x, y].gameObject.GetComponent<cDestroyAfter>().DestrAfter(0);
            }
        }
        for (int x = 0; x < GL.MaxX; x++) {
            for (int y = 0; y < GL.MaxY; y++) {
                SwapRandom(x, y);
                aChips[x, y] = null;
            }
        }
        RefreshField();
    }

    IEnumerator DoSwap() {// меняем фишки местами
        Vector2 tmpV2 = SelectedChip.transform.localPosition;
        Vector2 tmpToV2 = ToSwapChip.transform.localPosition;
        Vector2 tmpSelectV2 = SelectedChip.vFieldPosition;
        Vector2 tmpSwapV2 = ToSwapChip.vFieldPosition;
        SelectedChip.SetTarget(tmpToV2, tmpSwapV2);
        ToSwapChip.SetTarget(tmpV2, tmpSelectV2);
        while (!AllChipsMoved())  yield return new WaitForSeconds(.2f);
        //уничтожаем по возможности ряды 
        bool bChip1 = CheckKillRow(SelectedChip);
        bool bChip2 = CheckKillRow(ToSwapChip);
        if (!bChip1 && !bChip2) {//иначе возвращаем на исходные позиции
            SelectedChip.SetTarget(tmpV2, tmpSelectV2);
            ToSwapChip.SetTarget(tmpToV2, tmpSwapV2);
        }
        while (!AllChipsMoved()) yield return new WaitForSeconds(.2f);
        SelectedChip = null;
        ToSwapChip = null;
        yield return new WaitForSeconds(.2f);
        bool bAllReady = false;
       // bool bAllChecked = false;
        while (!bAllReady) {
            while (!AllChipsFilled()) { //пока есть незаполненные фишки 
                                        //сдвигаем нижние на пустые места на одну клетку
                MoveToUp();
                yield return new WaitForSeconds(.01f);
                while (!AllChipsMoved()) yield return new WaitForSeconds(.1f);
                AddNew();
            }
            yield return new WaitForSeconds(.01f);
            bAllReady = AllReady();
            yield return new WaitForSeconds(.01f);
        }
        while (!NoRows()) { //ищем невозможность образования ряда
            DoMix();//перемешиваем поле, если совпадения невозможны
            yield return new WaitForSeconds(.2f);
        }
        GL.GameState = eGameState.Play;
    }

    void MoveToUp() {//сдвигаем вертикальный ряд на пустые вверх на один
        List<int> MovedX = new List<int>();
        GL.GameState = eGameState.MoveUp;
        for (int x = 0; x < GL.MaxX; x++) {
            for (int y = GL.MaxY - 1; y >= 0; y--) {
                if (GL.pSave.aField[x, y] == -1 && !MovedX.Contains(x)) {//сдвигаем колонну на 1
                    MovedX.Add(x);
                    for (int j = y - 1; j >= 0; j--) {
                        if (GL.pSave.aField[x, j] != -1) {
                            aChips[x, j].SetTarget(new Vector2(x * 62, (j + 1) * 62), new Vector2(x, j + 1));
                            GL.pSave.aField[x, j] = -1;
                        }
                        if (j == 0) {//нижний заполняем случайной фишкой
                            GL.pSave.aField[x, j] = -2;
                        } 
                    }
                }
                if (y == 0 && GL.pSave.aField[x, y] == -1) {//нижний заполняем случайной фишкой
                    GL.pSave.aField[x, y] = -2;
                }

            }
        }
        GL.GameState = eGameState.Swap;
    }

    void AddNew() {//нижний заполняем случайной фишкой
        GL.GameState = eGameState.MoveUp;
        for (int x = 0; x < GL.MaxX; x++) {
            if (GL.pSave.aField[x, 0] == -2 ) {
                GL.pSave.aField[x, 0] = GetRandomChip(x, 0);
                SpawnChip(x, 0);
            }
        }
        GL.GameState = eGameState.Swap;
    }

    bool AllReady() {//проверяем на совпадения весь массив
        for (int x = 0; x < GL.MaxX; x++) {
            for (int y = 0; y < GL.MaxY; y++) {
                if (CheckKillRow(aChips[x, y])) return false;
            }
        }
        return true;
    }

    bool CheckKillRow(cChip Chip) {//проверяем на возможность удаления и удаляем ряд
        List<Vector2> lstForDelHorizon = new List<Vector2>();
        List<Vector2> lstForDelVertical = new List<Vector2>();
        lstForDelHorizon.Add(new Vector2(Chip.vFieldPosition.x, Chip.vFieldPosition.y));
        lstForDelVertical.Add(new Vector2(Chip.vFieldPosition.x, Chip.vFieldPosition.y));
        bool bKillLeft =true;
        bool bKillRight = true;
        for (int i = 1; i <= 4; i++) {
            if (bKillLeft) {//проверяем влево
                int xx = (int)Chip.vFieldPosition.x - i;
                int yy = (int)Chip.vFieldPosition.y;
                if (xx >= 0) {
                    if (GL.pSave.aField[xx, yy] == Chip.ChipId)
                        lstForDelHorizon.Add(new Vector2(xx, yy));
                    else
                        bKillLeft = false;
                } else bKillLeft = false;
            }
            if (bKillRight) {//проверяем вправо
                int xx = (int)Chip.vFieldPosition.x + i;
                int yy = (int)Chip.vFieldPosition.y;
                if (xx < GL.MaxX) {
                    if (GL.pSave.aField[xx, yy] == Chip.ChipId)
                        lstForDelHorizon.Add(new Vector2(xx, yy));
                    else
                        bKillRight = false;
                } else bKillRight = false;
            }
        }

        if (lstForDelHorizon.Count >= 3) {//если три и более в ряд, то удаляем
            foreach (var item in lstForDelHorizon) {
                aChips[(int)item.x, (int)item.y].gameObject.GetComponent<cDestroyAfter>().DestrAfter(0);
                GL.pSave.aField[(int)item.x, (int)item.y] = -1;
            }
            GL.pGameCtl.ScoreAdd(10);//подсчитываем очки
            if (lstForDelHorizon.Count >= 4) GL.pGameCtl.ScoreAdd((lstForDelHorizon.Count - 3) * 5);
            return true;
        }

        bool bKillUp = true;
        bool bKillDown = true;
        for (int i = 1; i <= 4; i++) {
            if (bKillUp) {//проверяем вверх
                int xx = (int)Chip.vFieldPosition.x;
                int yy = (int)Chip.vFieldPosition.y - i;
                if (yy >= 0) {
                    if (GL.pSave.aField[xx, yy] == Chip.ChipId)
                        lstForDelVertical.Add(new Vector2(xx, yy));
                    else
                        bKillUp = false;
                } else bKillUp = false;
            }
            if (bKillDown) {//проверяем вниз
                int xx = (int)Chip.vFieldPosition.x;
                int yy = (int)Chip.vFieldPosition.y + i;
                if (Chip.vFieldPosition.y + i < GL.MaxY) {
                    if (GL.pSave.aField[xx, yy] == Chip.ChipId)
                        lstForDelVertical.Add(new Vector2(xx, yy));
                    else
                        bKillDown = false;
                } else bKillDown = false;
            }
        }

        if (lstForDelVertical.Count >= 3) {//если три и более в ряд, то удаляем
            foreach (var item in lstForDelVertical) {
                aChips[(int)item.x, (int)item.y].gameObject.GetComponent<cDestroyAfter>().DestrAfter(0);
                GL.pSave.aField[(int)item.x, (int)item.y] = -1;

            }
            GL.pGameCtl.ScoreAdd(10);//подсчитываем очки
            if (lstForDelVertical.Count >= 4) GL.pGameCtl.ScoreAdd((lstForDelVertical.Count - 3) * 5);
            return true;
        }

        return false;
    }
}
