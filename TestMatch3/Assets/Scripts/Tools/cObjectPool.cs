﻿using UnityEngine;
using System.Collections.Generic;

public class cObjectPool : MonoBehaviour {//класс для кэширования объектов. Контроллер.

    GameObject prefab;
	public cPoolCtl prefabPoolCtl;

	List<cPoolCtl> availableObjects = new List<cPoolCtl>();

	public static cObjectPool GetPool (cPoolCtl cprefab) {
		GameObject obj;
		cObjectPool pool;
		if (Application.isEditor) {
			obj = GameObject.Find(cprefab.gameObject.name + " Pool");
			if (obj) {
				pool = obj.GetComponent<cObjectPool>();
				if (pool) {
					return pool;
				}
			}
		}
		obj = new GameObject(cprefab.gameObject.name + " Pool");
		pool = obj.AddComponent<cObjectPool>();
		pool.prefab = cprefab.gameObject;
		pool.prefabPoolCtl = cprefab;
		return pool;
	}

	public GameObject GetObject () {
		cPoolCtl obj;
		GameObject tmpGO;
		int lastAvailableIndex = availableObjects.Count - 1;
		if (lastAvailableIndex >= 0) {
			obj = availableObjects[lastAvailableIndex];
			availableObjects.RemoveAt(lastAvailableIndex);
			tmpGO = obj.gameObject;
		}
		else {
			tmpGO = Instantiate<GameObject>(prefab.gameObject);
			obj = tmpGO.GetComponent<cPoolCtl>();
			obj.transform.SetParent(transform, false);
			obj.Pool = this;
		}
		return tmpGO;
	}

	public void AddObject (cPoolCtl obj) {
		obj.gameObject.SetActive(false);
		obj.transform.parent = this.transform;
		availableObjects.Add(obj);
	}
}