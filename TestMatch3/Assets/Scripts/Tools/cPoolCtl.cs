﻿using UnityEngine;

public class cPoolCtl : MonoBehaviour {//класс для кэширования объектов. Хранитель.

	[System.NonSerialized]
	cObjectPool poolInstanceForPrefab;

	public GameObject GetPooledInstance(cPoolCtl cParentPool) {
		if (!poolInstanceForPrefab) {
			if (cParentPool == null)
				poolInstanceForPrefab = cObjectPool.GetPool(this);
			else 
				poolInstanceForPrefab = cObjectPool.GetPool(cParentPool.Pool.prefabPoolCtl);
		}
		return poolInstanceForPrefab.GetObject();
	}

	public cObjectPool Pool { get; set; }

	public void ReturnToPool () {
		if (Pool) {
			Pool.AddObject(this);
		}
		else {
			Destroy(gameObject);
		}
	}
}