﻿using UnityEngine;
using System.Collections;

public class cDestroyAfter : MonoBehaviour {//данный класс удаляет объект через определённое время, если тот не содержит кэширование
	cPoolCtl CurrPool;
	AudioSource CurrAudioCtl;

	public void DestrAfter (float cDestrTime) {
		CurrPool = gameObject.GetComponent<cPoolCtl>	();
		CurrAudioCtl = gameObject.GetComponent<AudioSource>	();
		StartCoroutine (ieDestrAfter(cDestrTime));
	}

	IEnumerator ieDestrAfter (float cDestrTime) {
		yield return new WaitForSeconds (cDestrTime);

		if (CurrPool) {
			CurrPool.ReturnToPool ();
		} else {
			Destroy (this.gameObject);
		}
	}

}
