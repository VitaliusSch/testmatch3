using UnityEngine;
using Zenject;

[CreateAssetMenu(fileName = "ConfigInstaller", menuName = "Installers/ConfigInstaller")]
public class cConfigInstaller : ScriptableObjectInstaller<cConfigInstaller>
{
    [SerializeField]
    public cConfig Config;

    public override void InstallBindings()
    {
        Container.BindInstance(Config);
    }
}