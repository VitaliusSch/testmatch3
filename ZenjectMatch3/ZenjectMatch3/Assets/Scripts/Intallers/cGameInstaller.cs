﻿using UnityEngine;
using Zenject;

public class cGameInstaller : MonoInstaller<cGameInstaller> {

    [Inject] cConfig Config;

    public override void InstallBindings() {

        Container.Bind<cGame>().AsSingle();
        Container.Bind<cFieldCore>().AsSingle();

        Container.Bind<fChipCtlFactory>().AsSingle();

        Container.DeclareSignal<EndMoveSignal>();
        Container.DeclareSignal<AddScoreSignal>();
    }
    

}
