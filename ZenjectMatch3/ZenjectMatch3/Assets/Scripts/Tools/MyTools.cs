﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class MyTools {

    public static T GetRandomEnum<T>(int cFrom = 0, int cTo = 1000)
    {
        System.Array A = System.Enum.GetValues(typeof(T));
        T V = (T)A.GetValue(UnityEngine.Random.Range(cFrom, Mathf.Min(cTo,A.Length)));
        return V;
    }

    public static T GetNextEnum<T>(string cFrom = "")
    {
        System.Array A = System.Enum.GetValues(typeof(T));
        bool bFind = false;
        foreach (var item in A)
        {
            if (bFind) return (T)item;
            if (item.ToString() == cFrom) bFind = true;
        }
        return (T)A.GetValue(0);
    }




}

public class cCoord {
    public int x;
    public int y;
    public cCoord() { }
    public cCoord(int pX, int pY) {
        x = pX;
        y = pY;
    }
}

