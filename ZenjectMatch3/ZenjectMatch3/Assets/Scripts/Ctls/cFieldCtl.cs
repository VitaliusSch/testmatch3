﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;


public class cFieldCtl : MonoBehaviour {


    [Inject] cConfig Config;
    [Inject] fChipCtlFactory ChipCtlFactory;
    [Inject] cFieldCore FieldCore;
    [Inject] cGame Game;
    [Inject] EndMoveSignal OnEndMoveSignal;

    public cChipCtl[,] aField;

    public Transform trParent;

    public cChipCtl SelectedChip;
    public cChipCtl ToSwapChip;

    public void Init() {
        SelectedChip = null;
        ToSwapChip = null;
        aField = new cChipCtl[Config.MaxX, Config.MaxY];
        for (int x = 0; x < Config.MaxX; x++) {
            for (int y = 0; y < Config.MaxY; y++) {
                eChipType tmpChipType = MyTools.GetRandomEnum<eChipType>();
                while (!CanUse(x, y, tmpChipType)) tmpChipType = MyTools.GetRandomEnum<eChipType>();
                cChipCtl tmpChip = ChipCtlFactory.Create(x, y, trParent, tmpChipType);
                aField[x, y] = tmpChip;
            }
        }

    }

    bool CanUse(int x, int y, eChipType ChipType) {// проверяем совпадение с соседями у заданной фишки
        bool tmpB = true;
        if (x - 1 >= 0) {
            if (aField[x - 1, y] != null)
            if (aField[x - 1, y].Chip.ChipType == ChipType) tmpB = false;
        }
        if (x + 1 < Config.MaxX) {
            if (aField[x + 1, y] != null)
            if (aField[x + 1, y].Chip.ChipType == ChipType) tmpB = false;
        }
        if (y - 1 >= 0) {
            if (aField[x, y - 1] != null)
            if (aField[x, y - 1].Chip.ChipType == ChipType) tmpB = false;
        }
        if (y + 1 < Config.MaxY) {
            if (aField[x, y + 1] != null)
            if (aField[x, y + 1].Chip.ChipType == ChipType) tmpB = false;
        }

        return tmpB;
    }

    public void CheckSelect(cChipCtl NewChip) {// Проверяем выделенную фишку
        if (SelectedChip == null) {//если текущая не выбрана, то выбираем
            SelectedChip = NewChip;
            return;
        }

        if (SelectedChip != null && FieldCore.CanSwap(SelectedChip, NewChip)) {//если текущая выбрана и нажата соседняя, то свапаем
            ToSwapChip = NewChip;
            SelectedChip.UnSelect();
            ToSwapChip.UnSelect();
            Game.GameState = eGameState.Swap;
            StartCoroutine(DoSwap());
        } else {//иначе выбираем новую
            SelectedChip.UnSelect();
            SelectedChip = NewChip;
        }

    }

    IEnumerator DoSwap() {// меняем фишки местами
        cCoord tmpSelectV2 = SelectedChip.Chip.Coord;
        cCoord tmpSwapV2 = ToSwapChip.Chip.Coord;
        ToSwapChip.SetTarget(tmpSelectV2);
        SelectedChip.SetTarget( tmpSwapV2);

        while (!FieldCore.AllChipsMoved()) yield return new WaitForSeconds(.01f);
        //уничтожаем по возможности ряды 
        bool bChip1 = FieldCore.CheckKillRow(SelectedChip);
        bool bChip2 = FieldCore.CheckKillRow(ToSwapChip);
        if (!bChip1 && !bChip2) {//иначе возвращаем на исходные позиции
            SelectedChip.SetTarget( tmpSelectV2);
            ToSwapChip.SetTarget( tmpSwapV2);
       }
        while (!FieldCore.AllChipsMoved()) yield return new WaitForSeconds(.01f);
        SelectedChip = null;
        ToSwapChip = null;

        bool bAllReady = false;
        while (!bAllReady) {
            while (!FieldCore.AllChipsFilled()) { //пока есть незаполненные фишки 
                                                  //сдвигаем нижние на пустые места на одну клетку
                FieldCore.MoveToUp();
                while (!FieldCore.AllChipsMoved()) yield return new WaitForSeconds(.1f);
                AddNew();
            }
            bAllReady = FieldCore.AllReady();
        }
        //while (!FieldCore.NoRows()) { //ищем невозможность образования ряда
        //    FieldCore.DoMix();//перемешиваем поле, если совпадения невозможны
        //    yield return new WaitForSeconds(.2f);
        //}
        yield return new WaitForSeconds(.01f);
        Game.GameState = eGameState.Play;
    }

    private void OnDisable() {
        OnEndMoveSignal.Unlisten(OnEndMove);
    }

    private void OnEnable() {
        OnEndMoveSignal.Listen(OnEndMove);
    }

    public void OnEndMove(cChipCtl ChipCtl) {//после падения очередной фишки, проверяем все ли фишки закончили движение
        if (FieldCore.AllChipsMoved()) Game.GameState = eGameState.Play;
    }


    void AddNew() {//нижний заполняем случайной фишкой
        Game.GameState = eGameState.MoveUp;
        for (int x = 0; x < Config.MaxX; x++) {
            if (aField[x, 0] == null) {
                eChipType tmpChipType = MyTools.GetRandomEnum<eChipType>();
                cChipCtl tmpChip = ChipCtlFactory.Create(x, 0, trParent, tmpChipType);
                aField[x, 0] = tmpChip;
            }
        }
        Game.GameState = eGameState.Swap;
    }



}

