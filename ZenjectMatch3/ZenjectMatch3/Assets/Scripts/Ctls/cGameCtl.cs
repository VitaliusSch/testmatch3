﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using UnityEngine.UI;

public class cGameCtl : MonoBehaviour {

    [Inject] cGame Game;
    [Inject] cFieldCtl FieldCtl;
    [Inject] AddScoreSignal OnAddScoreSignal;

    public Text teScore;

    void Start () {
        Game.Init();
        teScore.text = "0";
        FieldCtl.Init();
        Game.GameState = eGameState.Play;
    }

    public void ScoreAdd(int AddCnt) {
        int Key = 23 * 5 + 78;
        Game.Score += AddCnt * Key; //защищаем память от взлома
        teScore.text = (Game.Score / Key).ToString();
    }

    private void OnDisable() {
        OnAddScoreSignal.Unlisten(ScoreAdd);
    }

    private void OnEnable() {
        OnAddScoreSignal.Listen(ScoreAdd);
    }


}
