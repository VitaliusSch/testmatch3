﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using DG.Tweening;


public class cChipCtl : MonoBehaviour {

    [Inject] cGame Game;
    [Inject] cFieldCtl FieldCtl;
    [Inject] cConfig Config;
    [Inject] EndMoveSignal OnEndMoveSignal;

    Sequence mySequence;

    public GameObject SelectImg;
    public cChip Chip;
    GameObject go;
    Transform tr;

    public void Init() {
        Chip = new cChip();
        SelectImg.SetActive(false);
        go = gameObject;
        tr = transform;
    }

    public void Select() {
        if (Game.GameState != eGameState.Play) return;
        SelectImg.SetActive(true);
        FieldCtl.CheckSelect(this);
    }

    public void UnSelect() {
        if (Game.GameState != eGameState.Play) return;
        SelectImg.SetActive(false);
    }

    public void SetTarget(cCoord Coord) {//устанавливаем место назначения
        Chip.vTarget = new Vector2(Coord.x * Config.Step, Coord.y * Config.Step);
        FieldCtl.aField[Coord.x, Coord.y] = this;
        Chip.Coord = Coord;
        Chip.bMoved = false;
        mySequence = DOTween.Sequence();
        mySequence.Append(tr.DOLocalMove(Chip.vTarget, 0.1f))
                  .OnComplete(() => { Chip.bMoved = true;
                                      OnEndMoveSignal.Fire(this); });
    }

}


public class fChipCtlFactory {

    [Inject] cConfig Config;
    [Inject] DiContainer Container;

    public cChipCtl Create(int X, int Y, Transform trParent, eChipType pChipType) {

        GameObject tmpPrefab = Config.preChips[(int)pChipType];

        cChipCtl tmpChip = Container.InstantiatePrefabForComponent<cChipCtl>(tmpPrefab, trParent);
        tmpChip.Init();
        tmpChip.Chip.Coord = new cCoord(X, Y);
        tmpChip.Chip.ChipType = pChipType;
        tmpChip.Chip.bMoved = true;
        tmpChip.transform.localPosition = new Vector2(X * Config.Step, Y * Config.Step);
        return tmpChip;

    }

    public void DestroyChip(cChipCtl ChipCtl) {//задел для пула
        GameObject.DestroyObject(ChipCtl.gameObject);
    }
}

