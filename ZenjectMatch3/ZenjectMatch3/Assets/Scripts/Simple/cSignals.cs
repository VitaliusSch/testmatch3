﻿using Zenject;

public class EndMoveSignal : Signal<EndMoveSignal, cChipCtl> { }
public class AddScoreSignal : Signal<AddScoreSignal, int> { }