﻿using UnityEngine;

[CreateAssetMenu(fileName = "Config", menuName = "Create config")]
public class cConfig : ScriptableObject {

    public int MaxX = 6;
    public int MaxY = 6;
    public int Step = 6;

    public GameObject[] preChips;

}
