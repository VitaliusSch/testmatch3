﻿using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class cFieldCore  {

    [Inject] cConfig Config;
    [Inject] cFieldCtl FieldCtl;
    [Inject] cGame Game;
    [Inject] fChipCtlFactory ChipCtlFactory;
    [Inject] AddScoreSignal OnAddScoreSignal;

    public bool CheckBoard(int x, int y) { //проверяем границы
        if (x >= 0 || x < Config.MaxX || y >= 0 || y < Config.MaxY) return true;
        else return false;
    }


    public bool CanSwap(cChipCtl Chip1, cChipCtl Chip2) {// проверяем являются ли фишки соседями
        bool tmpB = false;
        if (Chip1.Chip.Coord.x == Chip2.Chip.Coord.x && Mathf.Abs(Chip1.Chip.Coord.y - Chip2.Chip.Coord.y) == 1) tmpB = true;
        if (Chip1.Chip.Coord.y == Chip2.Chip.Coord.y && Mathf.Abs(Chip1.Chip.Coord.x - Chip2.Chip.Coord.x) == 1) tmpB = true;
        return tmpB;
    }

    public bool NoRows() {//проверяем на невозможность образования ряда
        for (int x = 0; x < Config.MaxX; x++) {
            for (int y = 0; y < Config.MaxY; y++) {
                if (CheckPattern(x, y, x - 1, y + 1, x - 1, y - 1)) return true;//проверяем паттерн на одну клетку влево
                if (CheckPattern(x, y, x - 2, y + 1, x - 2, y + 2) || CheckPattern(x, y, x - 2, y - 1, x - 2, y - 2)) return true;//проверяем паттерны на две клетки влево
                if (CheckPattern(x, y, x - 2, y + 2, x - 3, y + 2) || CheckPattern(x, y, x - 2, y - 2, x - 3, y - 2) || CheckPattern(x, y, x - 2, y, x - 3, y)) return true;//проверяем паттерны на три клетки влево
                if (CheckPattern(x, y, x + 1, y + 1, x + 1, y - 1)) return true;//проверяем паттерн на одну клетку вправо
                if (CheckPattern(x, y, x + 2, y + 1, x + 2, y + 2) || CheckPattern(x, y, x + 2, y - 1, x + 2, y - 2)) return true;//проверяем паттерны на две клетки вправо
                if (CheckPattern(x, y, x + 2, y + 2, x + 3, y + 2) || CheckPattern(x, y, x + 2, y - 2, x + 3, y - 2) || CheckPattern(x, y, x + 2, y, x + 3, y)) return true;//проверяем паттерны на три клетки вправо
                if (CheckPattern(x, y, x + 1, y + 1, x - 1, y + 1)) return true;//проверяем паттерн на одну клетку вверх
                if (CheckPattern(x, y, x, y + 2, x, y + 3)) return true;//проверяем паттерн три клетки вверх
                if (CheckPattern(x, y, x + 1, y - 1, x - 1, y - 1)) return true;//проверяем паттерн на одну клетку вниз
                if (CheckPattern(x, y, x, y - 2, x, y - 3)) return true;//проверяем паттерн три клетки вниз
            }
        }
        return false;
    }

    bool CheckPattern(int x, int y, int x1, int y1, int x2, int y2) {
        if (x1 >= Config.MaxX || x2 >= Config.MaxX || x1 < 0 || x2 < 0 || y1 >= Config.MaxY || y2 >= Config.MaxY || y1 < 0 || y2 < 0) return false; //если выходим за границы, то нет совпадений

        if (FieldCtl.aField[x, y] == FieldCtl.aField[x1, y1] && FieldCtl.aField[x, y] == FieldCtl.aField[x2, y2]) return true;//сравниваем ИД проверяемых фишек
        else return false;
    }

    public bool CheckKillRow(cChipCtl ChipCtl) {//проверяем на возможность удаления и удаляем ряд
        List<Vector2> lstForDelHorizon = new List<Vector2>();
        List<Vector2> lstForDelVertical = new List<Vector2>();
        lstForDelHorizon.Add(new Vector2(ChipCtl.Chip.Coord.x, ChipCtl.Chip.Coord.y));
        lstForDelVertical.Add(new Vector2(ChipCtl.Chip.Coord.x, ChipCtl.Chip.Coord.y));
        bool bKillLeft = true;
        bool bKillRight = true;
        for (int i = 1; i <= 4; i++) {
            if (bKillLeft) {//проверяем влево
                int xx = (int)ChipCtl.Chip.Coord.x - i;
                int yy = (int)ChipCtl.Chip.Coord.y;
                if (xx >= 0) {
                    if (FieldCtl.aField[xx, yy] != null) {
                        if (FieldCtl.aField[xx, yy].Chip.ChipType == ChipCtl.Chip.ChipType)
                            lstForDelHorizon.Add(new Vector2(xx, yy));
                        else
                            bKillLeft = false;
                    }
                } else bKillLeft = false;
            }
            if (bKillRight) {//проверяем вправо
                int xx = (int)ChipCtl.Chip.Coord.x + i;
                int yy = (int)ChipCtl.Chip.Coord.y;
                if (xx < Config.MaxX) {
                    if (FieldCtl.aField[xx, yy] != null) {
                        if (FieldCtl.aField[xx, yy].Chip.ChipType == ChipCtl.Chip.ChipType)
                            lstForDelHorizon.Add(new Vector2(xx, yy));
                        else
                            bKillRight = false;
                    }
                } else bKillRight = false;
            }
        }

        if (lstForDelHorizon.Count >= 3) {//если три и более в ряд, то удаляем
            foreach (var item in lstForDelHorizon) {
                ChipCtlFactory.DestroyChip(FieldCtl.aField[(int)item.x, (int)item.y]);
                FieldCtl.aField[(int)item.x, (int)item.y] = null;
            }
            OnAddScoreSignal.Fire(10);//подсчитываем очки
            if (lstForDelHorizon.Count >= 4) OnAddScoreSignal.Fire((lstForDelHorizon.Count - 3) * 5);
            return true;
        }

        bool bKillUp = true;
        bool bKillDown = true;
        for (int i = 1; i <= 4; i++) {
            if (bKillUp) {//проверяем вверх
                int xx = (int)ChipCtl.Chip.Coord.x;
                int yy = (int)ChipCtl.Chip.Coord.y - i;
                if (yy >= 0) {
                    if (FieldCtl.aField[xx, yy] != null) {
                        if (FieldCtl.aField[xx, yy].Chip.ChipType == ChipCtl.Chip.ChipType)
                            lstForDelVertical.Add(new Vector2(xx, yy));
                        else
                            bKillUp = false;
                    }
                } else bKillUp = false;
            }
            if (bKillDown) {//проверяем вниз
                int xx = (int)ChipCtl.Chip.Coord.x;
                int yy = (int)ChipCtl.Chip.Coord.y + i;
                if (ChipCtl.Chip.Coord.y + i < Config.MaxY) {
                    if (FieldCtl.aField[xx, yy] != null) {
                        if (FieldCtl.aField[xx, yy].Chip.ChipType == ChipCtl.Chip.ChipType)
                            lstForDelVertical.Add(new Vector2(xx, yy));
                        else
                            bKillDown = false;
                    }
                } else bKillDown = false;
            }
        }

        if (lstForDelVertical.Count >= 3) {//если три и более в ряд, то удаляем
            foreach (var item in lstForDelVertical) {
                ChipCtlFactory.DestroyChip(FieldCtl.aField[(int)item.x, (int)item.y]);
                FieldCtl.aField[(int)item.x, (int)item.y] = null;

            }
            OnAddScoreSignal.Fire(10);//подсчитываем очки
            if (lstForDelVertical.Count >= 4) OnAddScoreSignal.Fire((lstForDelVertical.Count - 3) * 5);
            return true;
        }

        return false;
    }


    public bool AllReady() {//проверяем на совпадения весь массив
        for (int x = 0; x < Config.MaxX; x++) {
            for (int y = 0; y < Config.MaxY; y++) {
                if (CheckKillRow(FieldCtl.aField[x, y])) return false;
            }
        }
        return true;
    }

    public bool AllChipsFilled() {//проверяем все ли фишки заполненны
        bool tmpB = true;
        for (int x = 0; x < Config.MaxX; x++) {
            for (int y = 0; y < Config.MaxY; y++) {
                if (FieldCtl.aField[x, y] == null) tmpB = false;
            }
        }
        return tmpB;
    }

    public bool AllChipsMoved() {//проверяем все ли фишки закончили движение
        bool tmpB = true;
        for (int x = 0; x < Config.MaxX; x++) {
            for (int y = 0; y < Config.MaxY; y++) {
                if (FieldCtl.aField[x, y] != null) if (!FieldCtl.aField[x, y].Chip.bMoved) tmpB = false;
            }
        }
        return tmpB;
    }

    public void MoveToUp() {//сдвигаем вертикальный ряд на пустые вверх на один
        List<int> MovedX = new List<int>();
        Game.GameState = eGameState.MoveUp;
        for (int x = 0; x < Config.MaxX; x++) {
            for (int y = Config.MaxY - 1; y >= 0; y--) {
                if (FieldCtl.aField[x, y] == null && !MovedX.Contains(x)) {//сдвигаем колонну на 1
                    MovedX.Add(x);
                    for (int j = y - 1; j >= 0; j--) {
                        if (FieldCtl.aField[x, j] != null) {
                            //aChips[x, j].SetTarget(new Vector2(x * 62, (j + 1) * 62), new Vector2(x, j + 1));
                            FieldCtl.aField[x, j].SetTarget(new cCoord(x, (j + 1)));
                            FieldCtl.aField[x, j] = null;
                        }
                        if (j == 0 && FieldCtl.aField[x, y] == null) {//нижний заполняем случайной фишкой
                            FieldCtl.aField[x, j] = null;
                        }
                    }
                }
                if (y == 0 && FieldCtl.aField[x, y] == null) {//нижний заполняем случайной фишкой
                    FieldCtl.aField[x, y] = null;
                }

            }
        }
        Game.GameState = eGameState.Swap;
    }

}
